﻿namespace TourPlanner.Models {
    public class TourWithLogs {   //Structure of JSON file
        public Tour Tour { get; set; }
        public List<TourLog> Logs { get; set; }

        public void setTourValuesForImport() {
            Guid tourId = Guid.NewGuid();
            Tour.TourId = tourId;

            Logs = Logs == null ? new List<TourLog>() : Logs.ToList();
            Tour.calculatePopularity(Logs.Count);
            double diffAvg = Logs.Count > 0 ? Logs.Average(l => (int)l.Difficulty) : 0;
            double timeAvg = Logs.Count > 0 ? Logs.Average(l => l.TotalTime) : 0;
            Tour.calculateChildFriendliness(diffAvg, timeAvg);

            if (Logs.Count > 0)
                setLogValuesForImport();
        }

        private void setLogValuesForImport() {
            foreach (TourLog log in Logs) {
                log.TourId = Tour.TourId;
                log.LogId = Guid.NewGuid();
            }
        }
    }
}
