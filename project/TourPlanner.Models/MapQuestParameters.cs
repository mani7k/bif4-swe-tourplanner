﻿using System.Text.Json;
using System.Text.Json.Serialization;

namespace TourPlanner.Models {
    public class MapQuestParameters {

        public record Options {
            public string unit { get; set; } = "k";
            public TransportType routeType { get; set; } = TransportType.fastest;

        }

        public string[] locations { get; set; }
        public Options options { get; } = new Options();

        public MapQuestParameters(TransportType transportType, params string[] locations) {
            this.locations = locations;
            this.options.routeType = transportType;
        }

        [JsonIgnore]
        public StringContent content {
            get => new StringContent(JsonSerializer.Serialize(this));
        }
    }
}
