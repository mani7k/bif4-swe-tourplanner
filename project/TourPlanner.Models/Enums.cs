﻿using System.Text.Json.Serialization;

namespace TourPlanner.Models {
    public enum Diffculty {
        easy = 1,
        medium = 2,
        hard = 3,
        veryHard = 4
    }

    public enum ChildFriendliness {
        baby = 1,
        toddler = 2,
        child = 3,
        teenage = 4,
        adult = 5
    }

    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum TransportType {
        fastest = 1,
        shortest = 2,
        pedestrian = 3,
        bicycle = 4
    }
}
