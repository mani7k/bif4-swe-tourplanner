﻿namespace TourPlanner.Models {

    public record Route {
        public double distance { get; set; }
        public int time { get; set; }
        public string sessionId { get; set; }
    }

    public record MapQuestResult {
        public Route route { get; set; }
    }
}
