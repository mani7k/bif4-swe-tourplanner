﻿using Newtonsoft.Json;

namespace TourPlanner.Models {
    public class Tour {
        [JsonIgnore]
        public Guid TourId { get; set; }
        [JsonProperty(Required = Required.Always)]
        public string Name { get; set; }
        [JsonProperty(Required = Required.Always)]
        public string Description { get; set; }
        [JsonProperty(Required = Required.Always)]
        public string FromLocation { get; set; }
        [JsonProperty(Required = Required.Always)]
        public string ToLocation { get; set; }
        public string Path { get; set; }
        public double EstimatedTime { get; set; }
        public double TourDistance { get; set; }
        public int? Popularity { get; set; }
        [JsonProperty(Required = Required.Always)]
        public TransportType TransportType { get; set; }
        public ChildFriendliness? ChildFriendliness { get; set; }

        public Tour(Guid tourId, string name, string description, string fromLocation, string toLocation, string path,
            double estimatedTime, double tourDistance, TransportType transportType) {
            this.TourId = tourId;
            this.Name = name;
            this.Description = description;
            this.FromLocation = fromLocation;
            this.ToLocation = toLocation;
            this.Path = path;
            this.EstimatedTime = estimatedTime >= 0 ? estimatedTime : throw new ArgumentException("Estimated Time has to be greater 0!");
            this.TourDistance = tourDistance >= 0 ? tourDistance : throw new ArgumentException("Tour Distance has to be greater 0!");
            this.Popularity = null;
            this.TransportType = transportType;
            this.ChildFriendliness = null;
        }

        public override string ToString() => Name;

        public void calculateChildFriendliness(double difficultyAvg, double timeAvg) {
            if (difficultyAvg < 0 || difficultyAvg > 5) {
                throw new ArgumentException("Difficulty Average cannot be negative or greater 5!");
            } else if (timeAvg < 0) {
                throw new ArgumentException("Time Average cannot be negative!");
            }

            double result = (difficultyAvg / 2) + (timeAvg / 60) + this.TourDistance;    //diffulty * average of log times + distance of tour
            int childFriendlinessInt = ((result / 3) + 1 > 5) ? 5 : (int)((result / 3) + 1);
            this.ChildFriendliness = (ChildFriendliness)childFriendlinessInt;
        }

        public void calculatePopularity(int logCount) {
            if (logCount < 0) {
                throw new ArgumentException("There cannot be a negative LogCount!");
            }

            this.Popularity = (logCount / 5 + 1 > 5) ? 5 : logCount / 5 + 1;
        }
    }
}