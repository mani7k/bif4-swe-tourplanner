﻿using Newtonsoft.Json;

namespace TourPlanner.Models {
    public class TourLog {
        [JsonIgnore]
        public Guid LogId { get; set; }
        [JsonIgnore]
        public Guid TourId { get; set; }
        [JsonProperty(Required = Required.Always)]
        public DateTime StartingTime { get; set; }
        [JsonProperty(Required = Required.Always)]
        public string Comment { get; set; }
        [JsonProperty(Required = Required.Always)]
        public int TotalTime { get; set; }
        [JsonProperty(Required = Required.Always)]
        public int Rating { get; set; }
        [JsonProperty(Required = Required.Always)]
        public Diffculty Difficulty { get; set; }

        public TourLog(Guid logid, Guid tourId, DateTime startingTime, string comment, int totalTime, int rating, Diffculty difficulty) {
            this.LogId = logid;
            this.TourId = tourId;
            this.StartingTime = startingTime;
            this.Comment = comment;
            this.TotalTime = totalTime > 0 ? totalTime : throw new ArgumentException("Total Time has to be greater 0!");
            this.Rating = (rating >= 1 && rating <= 5) ? rating : throw new ArgumentException("Rating has to be between 1 and 5!");
            this.Difficulty = difficulty;
        }

        public override string ToString() {
            return $"TourLog(logid={LogId}, tourId={TourId}, startingTime={StartingTime}, comment={Comment}, totalTime={TotalTime}, rating={Rating}, difficulty={Difficulty})";
        }
    }
}
