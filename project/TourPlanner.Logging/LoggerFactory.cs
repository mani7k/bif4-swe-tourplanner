﻿namespace TourPlanner.Logging {
    public static class LoggerFactory {
        public static string configPath;
        public static ILoggerWrapper GetLogger() {
            return Log4NetWrapper.CreateLogger(configPath);
        }
    }
}
