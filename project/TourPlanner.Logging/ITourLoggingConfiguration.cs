﻿namespace TourPlanner.Logging {
    public interface ITourLoggingConfiguration {
        string LoggingFile { get; }
    }
}
