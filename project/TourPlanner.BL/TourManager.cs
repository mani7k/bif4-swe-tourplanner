﻿using TourPlanner.BL.Exceptions;
using TourPlanner.DAL;
using TourPlanner.DAL.Exceptions;
using TourPlanner.DAL.Files;
using TourPlanner.Logging;
using TourPlanner.Models;

namespace TourPlanner.BL {
    public class TourManager : ITourManager {
        private readonly ITourDAO dao;
        private readonly IFileDAO daoFiles;
        private readonly IMapQuestDao mapQuestDao;
        private readonly ILoggerWrapper logger;

        public TourManager(ITourDAO dao, FileDAO daoFiles, IMapQuestDao mapQuestDao) {
            this.dao = dao;
            this.daoFiles = daoFiles;
            this.mapQuestDao = mapQuestDao;
            this.logger = LoggerFactory.GetLogger();
        }
        public Task<IEnumerable<Tour>> getTours() => Task.Run(async () => {
            try {
                return dao.getTours();
            } catch (DataAccessFailedException dafe) {
                throw new ExternalAccessFailedException(dafe.Message);
            } catch (Exception) {
                throw;
            }
        });

        public Task<IEnumerable<Tour>> findMatchingTours(string text) => Task.Run(async () => {
            IEnumerable<Tour> tours = new List<Tour>();

            try {
                tours = dao.getTours().ToList<Tour>().FindAll(tour => {
                    // 1. search the tour attributes
                    var searchString = $"{tour.Name} {tour.Description} {tour.FromLocation} {tour.ToLocation}";
                    if (searchString.Contains(text.Trim())) {
                        return true;
                    }

                    // 2. if nothing found inside the tour attributes -> search through logs
                    var logs = dao.getLogsByTourId(tour.TourId);

                    foreach (var log in logs) {
                        searchString = $"{log.Comment} {log.StartingTime} {log.StartingTime:dd.MM.yyyy HH:mm}";

                        if (searchString.Contains(text.Trim())) {
                            return true;
                        }
                    }

                    // 3. if still nothing found -> falses
                    return false;
                }).AsEnumerable();
            } catch (DataAccessFailedException dafe) {
                throw new ExternalAccessFailedException(dafe.Message);
            } catch (Exception) {
                throw;
            }

            return tours;
        });

        public Task<IEnumerable<TourLog>> getTourLogs(Guid tourId) => Task.Run(async () => {
            try {
                return dao.getLogsByTourId(tourId);
            } catch (DataAccessFailedException dafe) {
                throw new ExternalAccessFailedException(dafe.Message);
            } catch (Exception) {
                throw;
            }
        });

        public Task<bool> addTour(Tour tour) => Task.Run(async () => {
            try {
                mapQuestCalculation(ref tour);

                bool isAdded = dao.insertTour(tour);
                if (isAdded) {
                    logger.Debug($"Inserted Tour with ID: {tour.TourId}");
                }

                return isAdded;
            } catch (DataAccessFailedException dafe) {
                throw new ExternalAccessFailedException(dafe.Message);
            } catch (MapQuestAccessFailedException mqafe) {
                throw new ExternalAccessFailedException(mqafe.Message);
            } catch (MapQuestNotDeserializableException mqnde) {
                throw new ExternalAccessFailedException(mqnde.Message);
            } catch (Exception) {
                throw;
            }
        });

        public Task<bool> editTour(Tour tour) => Task.Run(async () => {
            try {
                mapQuestCalculation(ref tour);

                bool isUpdated = dao.updateTour(tour);
                if (isUpdated) {
                    logger.Debug($"Updated Tour with ID: {tour.TourId}");
                }
                return isUpdated;
            } catch (DataAccessFailedException dafe) {
                throw new ExternalAccessFailedException(dafe.Message);
            } catch (Exception) {
                throw;
            }
        });

        public Task<bool> deleteTour(Guid tourId) => Task.Run(async () => {
            try {
                bool isDeleted = dao.deleteTour(tourId);
                if (isDeleted) {
                    logger.Debug($"Deleted Tour with ID: {tourId}");
                }
                return isDeleted;
            } catch (DataAccessFailedException dafe) {
                throw new ExternalAccessFailedException(dafe.Message);
            } catch (Exception) {
                throw;
            }
        });

        public Task<bool> addTourLog(TourLog tourLog) => Task.Run(async () => {
            try {
                bool isAdded = dao.insertLog(tourLog);
                if (isAdded) {
                    logger.Debug($"Inserted Log with ID: {tourLog.LogId}");
                }
                return isAdded;
            } catch (DataAccessFailedException dafe) {
                throw new ExternalAccessFailedException(dafe.Message);
            } catch (Exception) {
                throw;
            }
        });

        public Task<bool> editTourLog(TourLog tourLog) => Task.Run(async () => {
            try {
                bool isUpdated = dao.updateLog(tourLog);
                if (isUpdated) {
                    logger.Debug($"Updated Log with ID: {tourLog.LogId}");
                }
                return isUpdated;
            } catch (DataAccessFailedException dafe) {
                throw new ExternalAccessFailedException(dafe.Message);
            } catch (Exception) {
                throw;
            }
        });

        public Task<bool> deleteTourLog(Guid tourLogId) => Task.Run(async () => {
            try {
                bool isDeleted = dao.deleteLog(tourLogId);
                if (isDeleted) {
                    logger.Debug($"Deleted Log with ID: {tourLogId}");
                }
                return isDeleted;
            } catch (DataAccessFailedException dafe) {
                throw new ExternalAccessFailedException(dafe.Message);
            } catch (Exception) {
                throw;
            }
        });

        public Task<bool> importTour(string filename) => Task.Run(async () => {
            TourWithLogs root;
            try {
                root = daoFiles.importTour(filename);
            } catch (ParsingException pex) {
                throw new ImportExportException(pex.Message);
            }

            if (root == null) {
                throw new ImportExportException($"Could not import Tour with Logs from file: {filename}!");
            } else {
                bool res = false;
                try {
                    res = dao.insertTour(root.Tour);
                } catch (DataAccessFailedException dafe) {
                    throw new ExternalAccessFailedException($"Could not insert imported Tour with ID: {root.Tour.TourId}!");
                } catch (DMLException dafe) {
                    throw new ImportExportException(dafe.Message);
                }

                root.Logs.ForEach(l => res &= dao.insertLog(l));
                logger.Debug($"Inserted Tour with associated logs from file: {filename}");
                return res;
            }

        });

        public Task<bool> exportTour(string filename, Guid tourId) => Task.Run(async () => {
            bool res = false;

            try {
                Tour tour = dao.getTourById(tourId);
                IEnumerable<TourLog> logs = dao.getLogsByTourId(tourId);

                res = daoFiles.exportTour(filename, tour, logs);
                if (res == false) {
                    throw new ImportExportException($"Could not export Tour with Logs to file: {filename}!");
                }
            } catch (DataAccessFailedException dafe) {
                throw new ExternalAccessFailedException(dafe.Message);
            }

            logger.Debug($"Exported Tour with associated logs to file: {filename}");
            return res;
        });

        public Task<bool> generateTourReport(string filename, Guid tourId) => Task.Run(async () => {
            bool res = false;

            try {
                Tour tour = dao.getTourById(tourId);
                IEnumerable<TourLog> logs = dao.getLogsByTourId(tourId);

                res = daoFiles.generateTourReport(filename, tour, logs);
                if (res == false) {
                    throw new ReportGenerationFailedException($"Could not generate Report for tour {tour.Name}!");
                } 
            } catch (DataAccessFailedException dafe) {
                throw new ExternalAccessFailedException(dafe.Message);
            }

            logger.Debug($"Generated tour report for Tour with ID: {tourId} to {filename}!");
            return res;
        });

        public Task<bool> generateSummarizeReport(string filename) => Task.Run(async () => {
            bool res = false;
            List<TourWithLogs> toursWithLogs = new List<TourWithLogs>();

            try {
                foreach (Tour tour in dao.getTours()) {
                    TourWithLogs twl = new TourWithLogs();
                    List<TourLog> logs = dao.getLogsByTourId(tour.TourId).ToList();

                    double diffAvg = logs.Count > 0 ? logs.Average(l => (int)l.Difficulty) : 0;
                    double timeAvg = logs.Count > 0 ? logs.Average(l => l.TotalTime) : 0;
                    tour.calculatePopularity(logs.Count);
                    tour.calculateChildFriendliness(diffAvg, timeAvg);

                    twl.Tour = tour;
                    twl.Logs = logs;

                    toursWithLogs.Add(twl);
                }

                res = daoFiles.generateSummarizeReport(filename, toursWithLogs);
                if (res == false) {
                    throw new ReportGenerationFailedException($"Could not generate summarize report!");
                } 
            } catch (DataAccessFailedException dafe) {
                throw new ExternalAccessFailedException(dafe.Message);
            } catch (Exception) {
                throw;
            }

            logger.Debug($"Generated summarize report to {filename}!");
            return res;
        });

        private void mapQuestCalculation(ref Tour tour) {
            // Calculate distance/time
            var mapQuestResult = mapQuestDao.getMapQuestResult(tour.FromLocation, tour.ToLocation, tour.TransportType);
            tour.TourDistance = mapQuestResult.route.distance;
            tour.EstimatedTime = mapQuestResult.route.time / 60;

            // Get image and save path
            var path = mapQuestDao.saveMapQuestImage(mapQuestResult.route.sessionId);
            tour.Path = Path.GetFullPath(path);
        }
    }
}
