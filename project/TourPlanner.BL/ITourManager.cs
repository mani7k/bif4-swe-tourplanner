﻿using TourPlanner.Models;

namespace TourPlanner.BL {
    public interface ITourManager {
        Task<IEnumerable<Tour>> getTours();
        Task<IEnumerable<Tour>> findMatchingTours(string text);
        Task<IEnumerable<TourLog>> getTourLogs(Guid tourId);
        Task<bool> addTour(Tour tour);
        Task<bool> editTour(Tour tour);
        Task<bool> deleteTour(Guid tourId);
        Task<bool> addTourLog(TourLog tourLog);
        Task<bool> editTourLog(TourLog tourLog);
        Task<bool> deleteTourLog(Guid tourLogId);
        Task<bool> importTour(string filename);
        Task<bool> exportTour(string filename, Guid tourId);
        Task<bool> generateTourReport(string filename, Guid tourId);
        Task<bool> generateSummarizeReport(string filename);

    }
}