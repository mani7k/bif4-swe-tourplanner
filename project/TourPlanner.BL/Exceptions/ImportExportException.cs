﻿namespace TourPlanner.BL.Exceptions {
    public class ImportExportException : Exception {
        public ImportExportException() {
        }

        public ImportExportException(string? message) : base(message) {
        }

        public ImportExportException(string? message, Exception? innerException) : base(message, innerException) {
        }
    }
}
