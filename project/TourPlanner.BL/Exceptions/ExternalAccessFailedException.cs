﻿namespace TourPlanner.BL.Exceptions {
    public class ExternalAccessFailedException : Exception {
        public ExternalAccessFailedException() {
        }

        public ExternalAccessFailedException(string? message) : base(message) {
        }

        public ExternalAccessFailedException(string? message, Exception? innerException) : base(message, innerException) {
        }
    }
}
