﻿namespace TourPlanner.BL.Exceptions {
    public class ReportGenerationFailedException : Exception {
        public ReportGenerationFailedException() {
        }

        public ReportGenerationFailedException(string? message) : base(message) {
        }

        public ReportGenerationFailedException(string? message, Exception? innerException) : base(message, innerException) {
        }
    }
}
