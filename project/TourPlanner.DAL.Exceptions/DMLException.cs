﻿namespace TourPlanner.DAL.Exceptions {
    public class DMLException : Exception { //DML = Data Manipulation Language Exception
        public DMLException() {
        }

        public DMLException(string? message) : base(message) {
        }

        public DMLException(string? message, Exception? innerException) : base(message, innerException) {
        }
    }
}
