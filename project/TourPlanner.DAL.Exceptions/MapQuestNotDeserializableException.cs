﻿namespace TourPlanner.DAL.Exceptions {
    public class MapQuestNotDeserializableException : Exception {
        public MapQuestNotDeserializableException() {
        }

        public MapQuestNotDeserializableException(string? message) : base(message) {
        }

        public MapQuestNotDeserializableException(string? message, Exception? innerException) : base(message, innerException) {
        }
    }
}
