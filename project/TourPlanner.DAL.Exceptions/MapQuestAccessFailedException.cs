﻿namespace TourPlanner.DAL.Exceptions {
    public class MapQuestAccessFailedException : Exception {
        public MapQuestAccessFailedException() {
        }

        public MapQuestAccessFailedException(string? message) : base(message) {
        }

        public MapQuestAccessFailedException(string? message, Exception? innerException) : base(message, innerException) {
        }
    }
}
