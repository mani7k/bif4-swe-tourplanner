﻿using Newtonsoft.Json;
using System.Diagnostics;
using System.Drawing;
using System.Net.Http.Headers;
using TourPlanner.DAL.Exceptions;
using TourPlanner.Models;

namespace TourPlanner.DAL.MapQuest {
    public class MapQuestDao : IMapQuestDao {
        private readonly ITourMapQuestConfiguration configuration;
        private readonly HttpClient client = new HttpClient();
        private readonly JsonSerializer serializer = new JsonSerializer();

        public MapQuestDao(ITourMapQuestConfiguration configuration) {
            this.configuration = configuration;

            client.BaseAddress = new Uri("http://www.mapquestapi.com/");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public MapQuestResult getMapQuestResult(string start, string end, TransportType transportType) {
            var input = new MapQuestParameters(transportType, start, end);

            var response = client.PostAsync($"directions/v2/route?key={configuration.MapQuestKey}", input.content).Result;
            response.EnsureSuccessStatusCode();

            if (response.IsSuccessStatusCode) {

                var stream = response.Content.ReadAsStream();
                var sr = new StreamReader(stream);
                var jsonReader = new JsonTextReader(sr);

                var result = serializer.Deserialize<MapQuestResult>(jsonReader);
                if (result is null) {
                    throw new MapQuestNotDeserializableException("Could not deserialize MapQuest response!");
                } else {
                    return result;
                }
            } else {
                throw new MapQuestAccessFailedException("Could either not access MapQuest or invalid " +
                    "input parameters (transportType/start/end)!");
            }
        }

        public string saveMapQuestImage(string sessionId) {

            Debug.WriteLine("sessionId: " + sessionId);

            var response = client.GetAsync($"https://open.mapquestapi.com/staticmap/v5/map?session={sessionId}&size=@2x&key={configuration.MapQuestKey}").Result;
            response.EnsureSuccessStatusCode();

            if (response.IsSuccessStatusCode) {
                var img = Image.FromStream(response.Content.ReadAsStream());
                var filename = $"{img.GetHashCode()}.jpeg";
                img.Save(filename);

                return filename;
            } else {
                throw new MapQuestAccessFailedException("Could either not access MapQuest or invalid " +
                    "input parameters (transportType/start/end)!");
            }
        }
    }
}