﻿namespace TourPlanner.DAL {
    public interface IFileDAO : IImportExportTour, IReportGenerator {
    }
}
