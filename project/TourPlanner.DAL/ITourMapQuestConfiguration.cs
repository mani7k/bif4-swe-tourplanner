﻿namespace TourPlanner.DAL {
    public interface ITourMapQuestConfiguration {
        string MapQuestKey { get; }
    }
}
