﻿using TourPlanner.Models;

namespace TourPlanner.DAL {
    public interface IImportExportTour {
        TourWithLogs importTour(string filename);
        bool exportTour(string filename, Tour tour, IEnumerable<TourLog> logs);
    }
}
