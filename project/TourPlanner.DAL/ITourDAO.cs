﻿using TourPlanner.Models;

namespace TourPlanner.DAL {
    public interface ITourDAO {
        IEnumerable<Tour> getTours();
        Tour? getTourById(Guid id);
        bool insertTour(Tour tour);
        bool updateTour(Tour tour);
        bool deleteTour(Guid tourId);


        IEnumerable<TourLog> getLogsByTourId(Guid tourId);
        bool insertLog(TourLog log);
        bool updateLog(TourLog log);
        bool deleteLog(Guid logId);
    }
}
