﻿using TourPlanner.Models;

namespace TourPlanner.DAL {
    public interface IMapQuestDao {
        MapQuestResult getMapQuestResult(string start, string end, TransportType transportType);
        string saveMapQuestImage(string sessionId);
    }
}
