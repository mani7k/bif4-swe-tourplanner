﻿using TourPlanner.Models;

namespace TourPlanner.DAL {
    public interface IReportGenerator {
        bool generateTourReport(string filename, Tour tour, IEnumerable<TourLog> logs);
        bool generateSummarizeReport(string filename, IEnumerable<TourWithLogs> toursWithLogs);
    }
}
