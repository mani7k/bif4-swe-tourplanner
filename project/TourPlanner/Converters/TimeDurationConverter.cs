﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace TourPlanner.Converters {
    [ValueConversion(typeof(int), typeof(string))]
    public class TimeDurationConverter : IValueConverter {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            var ticks = System.Convert.ToInt64(value) * 60 * 10000000L;
            return new TimeSpan(ticks).ToString("c");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            throw new NotImplementedException();
        }
    }
}
