﻿using System;
using System.Windows.Data;

namespace TourPlanner.Converters {

    /**
     * This converter receives a list of booleans
     * return value:
     *     -) if one element is true -> false
     *     -) if all elements are false -> true
     *     
     *     this is used for MultiBindings to check wether there are validations with error
     *     and therefor disable e.g. a button
     */

    public class AndBooleanConverter : IMultiValueConverter {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
            if (values.LongLength > 0) {
                foreach (var value in values) {
                    if (value is bool && (bool)value) {
                        return false;
                    }
                }
            }
            return true;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture) {
            throw new NotImplementedException();
        }
    }
}
