﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace TourPlanner.Converters {
    [ValueConversion(typeof(DateTime), typeof(string))]
    public class StartTimeConverter : IValueConverter {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            var date = (DateTime)value;
            return date.ToString("HH:mm");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            throw new NotImplementedException();
        }
    }
}
