﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using TourPlanner.ViewModels;

namespace TourPlanner.Views {
    public partial class RatingInputView : UserControl, INotifyPropertyChanged {
        public RatingInputView() {
            InitializeComponent();

            OneStar = new RelayCommand((_) => Rating = 1);
            TwoStar = new RelayCommand((_) => Rating = 2);
            ThreeStar = new RelayCommand((_) => Rating = 3);
            FourStar = new RelayCommand((_) => Rating = 4);
            FiveStar = new RelayCommand((_) => Rating = 5);
        }

        public event PropertyChangedEventHandler? PropertyChanged;

        public object[] Res { get; set; } = new object[5] {
            "/Views/star_filled.png",
            "/Views/star_outlined.png",
            "/Views/star_outlined.png",
            "/Views/star_outlined.png",
            "/Views/star_outlined.png"
        };

        private void changeRating(int rating) {
            for (int i = 0; i < rating; i++) {
                Res[i] = "/Views/star_filled.png";
            }

            for (int i = 4; i >= rating; i--) {
                Res[i] = "/Views/star_outlined.png";
            }

            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Res"));
        }

        public ICommand OneStar { get; }
        public ICommand TwoStar { get; }
        public ICommand ThreeStar { get; }
        public ICommand FourStar { get; }
        public ICommand FiveStar { get; }

        readonly public static DependencyProperty RatingProperty =
        DependencyProperty.Register(
            name: "Rating",
            propertyType: typeof(int),
            ownerType: typeof(RatingInputView),
            typeMetadata: new FrameworkPropertyMetadata(
                defaultValue: 1,
                propertyChangedCallback: new PropertyChangedCallback((dp, _) => {
                    var curr = (RatingInputView)dp;
                    curr.changeRating(curr.Rating);
                })
            )
        );

        public int Rating {
            get => (int)GetValue(RatingProperty);
            set {
                SetValue(RatingProperty, value);
            }
        }
    }
}
