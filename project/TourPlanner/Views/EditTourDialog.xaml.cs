﻿using System.Windows;
using TourPlanner.ViewModels;

namespace TourPlanner.Views {
    public partial class EditTourDialog : Window {

        public EditTourDialog() {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e) {
            if (DataContext is ICloseWindow cw) {
                cw.Close += () => Close();
            }
        }
    }
}
