﻿using Microsoft.Extensions.Configuration;
using TourPlanner.DAL;
using TourPlanner.DAL.PostgreSql;
using TourPlanner.Logging;

namespace TourPlanner.Configuration {
    internal class AppConfiguration : ITourPostgreSQLDAOConfiguration, ITourMapQuestConfiguration, ITourLoggingConfiguration {
        private readonly IConfiguration configuration;
        public string LoggingFile => configuration["logging:file"];
        public string MapQuestKey => configuration["mapQuest:key"];
        public string ConnectionString => configuration["database:connectionstring"];

        public AppConfiguration(IConfiguration configuration) {
            this.configuration = configuration;
        }
    }
}
