﻿using Microsoft.Extensions.Configuration;
using System;
using System.Windows;
using TourPlanner.BL;
using TourPlanner.Configuration;
using TourPlanner.DAL.Files;
using TourPlanner.DAL.MapQuest;
using TourPlanner.DAL.PostgreSql;
using TourPlanner.Logging;
using TourPlanner.Navigation;
using TourPlanner.ViewModels;
using TourPlanner.Views;

namespace TourPlanner {
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application {
        private ILoggerWrapper logger;
        private void Application_Startup(object sender, StartupEventArgs e) {
            IConfiguration iconfig = new ConfigurationBuilder()
                    .AddJsonFile("appsettings.json", false, true)
                    .Build();

            AppConfiguration config = new AppConfiguration(iconfig);
            LoggerFactory.configPath = config.LoggingFile;

            logger = LoggerFactory.GetLogger();
            var dao = new TourPostgreSQLDAO(config);
            var fileDao = new FileDAO();
            var mapQuestDao = new MapQuestDao(config);
            var tourManager = new TourManager(dao, fileDao, mapQuestDao);

            var navigationService = new NavigationService();
            navigationService.RegisterNavigation<AddTourDialogViewModel, AddTourDialog>();
            navigationService.RegisterNavigation<AddLogDialogViewModel, AddLogDialog>();
            navigationService.RegisterNavigation<EditTourDialogViewModel, EditTourDialog>();
            navigationService.RegisterNavigation<EditLogDialogViewModel, EditLogDialog>();

            var searchViewModel = new SearchViewModel();
            var addTourDialogViewModel = new AddTourDialogViewModel();
            var addLogDialogViewModel = new AddLogDialogViewModel();
            var editTourDialogViewModel = new EditTourDialogViewModel();
            var editLogDialogViewModel = new EditLogDialogViewModel();
            var resultsViewModel = new ResultsViewModel(
                tourManager,
                addLogDialogViewModel,
                editLogDialogViewModel
            ) {
                NavigationService = navigationService
            };

            var mainViewModel = new MainViewModel(
                tourManager,
                addTourDialogViewModel,
                editTourDialogViewModel,
                searchViewModel,
                resultsViewModel
            ) {
                NavigationService = navigationService
            };

            var wnd = new MainWindow {
                DataContext = mainViewModel,
                SearchBar = { DataContext = searchViewModel },
                ResultView = { DataContext = resultsViewModel }
            };

            wnd.Show();
        }
    }
}