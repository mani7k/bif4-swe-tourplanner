using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using TourPlanner.BL;
using TourPlanner.BL.Exceptions;
using TourPlanner.Exceptions;
using TourPlanner.Models;
using TourPlanner.Util;

namespace TourPlanner.ViewModels {
    internal class ResultsViewModel : BaseViewModel {
        private readonly ITourManager tourManager;
        private readonly AddLogDialogViewModel addLogDialogViewModel;
        private readonly EditLogDialogViewModel editLogDialogViewModel;

        public ICommand AddLogCommand { get; }
        public ICommand EditLogCommand { get; }
        public ICommand DeleteLogCommand { get; }

        private NotifyTaskCompletion<IEnumerable<TourLog>> tourLogsAsync;
        public NotifyTaskCompletion<IEnumerable<TourLog>> TourLogsAsync {
            get => tourLogsAsync;
            private set {
                tourLogsAsync = value;
                OnPropertyChanged();
            }
        }

        private Tour? selectedItem;
        public Tour? SelectedItem {
            get => selectedItem;
            set {
                selectedItem = value;
                if (value != null) {
                    TourLogsAsync = new NotifyTaskCompletion<IEnumerable<TourLog>>(tourManager.getTourLogs(value.TourId));
                } else {
                    // todo show screen which indicates, that no tour has been selected

                    TourLogsAsync = new NotifyTaskCompletion<IEnumerable<TourLog>>(Task.Run(() => {
                        return new List<TourLog>().AsEnumerable();
                    }));
                }

                OnPropertyChanged();
            }
        }

        private TourLog? selectedLog;
        public TourLog? SelectedLog {
            get => selectedLog;
            set {
                selectedLog = value;
                editLogDialogViewModel.SelectedItem = value;
                OnPropertyChanged();
            }
        }

        public ResultsViewModel(
            ITourManager tourManager,
            AddLogDialogViewModel addLogDialogViewModel,
            EditLogDialogViewModel editLogDialogViewModel
        ) {
            this.tourManager = tourManager;
            this.addLogDialogViewModel = addLogDialogViewModel;
            this.editLogDialogViewModel = editLogDialogViewModel;

            AddLogCommand = new RelayCommand((_) => {
                NavigationService?.NavigateTo(addLogDialogViewModel);
            }, (_) => SelectedItem != null);

            EditLogCommand = new RelayCommand((_) => {
                NavigationService?.NavigateTo(editLogDialogViewModel);
            }, (_) => SelectedLog != null);

            DeleteLogCommand = new RelayCommand((_) => {
                var task = Task.Run(async () => {
                    try {
                        var res = await tourManager.deleteTourLog(SelectedLog.LogId);
                        Contract.Requires(res); // checks whether adding was successful
                    } catch (ExternalAccessFailedException) {
                        throw new ErrorException($"Unable to delete Log with ID: {SelectedLog.LogId}!");
                    } catch (Exception e) {
                        throw new FatalException($"Error in deleteLog: {e.Message}");
                    }

                    return await tourManager.getTourLogs(selectedItem.TourId);
                });

                TourLogsAsync = new NotifyTaskCompletion<IEnumerable<TourLog>>(task);
            }, (_) => SelectedLog != null);

            addLogDialogViewModel.LogInputCompleted += (_, tourLog) => {
                tourLog.TourId = SelectedItem.TourId;

                var task = Task.Run(async () => {
                    try {
                        var res = await tourManager.addTourLog(tourLog);
                        Contract.Requires(res); // checks whether adding was successful

                        return await tourManager.getTourLogs(selectedItem.TourId);
                    } catch (ExternalAccessFailedException eafe) {
                        throw new ErrorException($"Unable to add Log with ID: {tourLog.TourId}!");
                    } catch (Exception e) {
                        throw new FatalException($"Error in addLog: {e.Message}");
                    }
                });

                TourLogsAsync = new NotifyTaskCompletion<IEnumerable<TourLog>>(task);
            };

            editLogDialogViewModel.LogInputCompleted += (_, tourLog) => {
                var task = Task.Run(async () => {
                    try {
                        var res = await tourManager.editTourLog(tourLog);
                        Contract.Requires(res); // checks whether adding was successful
                        return await tourManager.getTourLogs(selectedItem.TourId);
                    } catch (ExternalAccessFailedException eafe) {
                        throw new ErrorException($"Unable to add Log with ID: {tourLog.TourId}!");
                    } catch (Exception e) {
                        throw new FatalException($"Error in editLog: {e.Message}");
                    }
                });

                TourLogsAsync = new NotifyTaskCompletion<IEnumerable<TourLog>>(task);
            };
        }
    }
}
