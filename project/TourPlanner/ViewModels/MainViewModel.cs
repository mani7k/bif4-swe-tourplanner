﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Threading.Tasks;
using System.Windows.Input;
using TourPlanner.BL;
using TourPlanner.BL.Exceptions;
using TourPlanner.Exceptions;
using TourPlanner.Models;
using TourPlanner.Util;

namespace TourPlanner.ViewModels {
    internal class MainViewModel : BaseViewModel, ICloseWindow {
        private readonly ITourManager tourManager;
        private readonly ResultsViewModel resultsViewModel;
        private readonly EditTourDialogViewModel editTourDialogViewModel;

        public ICommand AddTourCommand { get; }
        public ICommand EditTourCommand { get; }
        public ICommand DeleteTourCommand { get; }
        public ICommand ExitCommand { get; }
        public ICommand ImportCommand { get; }
        public ICommand ExportCommand { get; }
        public ICommand ReportTourCommand { get; }
        public ICommand ReportSummarizeCommand { get; }

        public Action? Close { get; set; }

        private Tour? selectedItem;
        public Tour? SelectedItem {
            get => selectedItem;
            set {
                selectedItem = value;
                resultsViewModel.SelectedItem = value;
                editTourDialogViewModel.SelectedItem = value;
                OnPropertyChanged();
            }
        }

        private NotifyTaskCompletion<IEnumerable<Tour>> tourAsync;
        public NotifyTaskCompletion<IEnumerable<Tour>> TourAsync {
            get => tourAsync;
            private set {
                tourAsync = value;
                OnPropertyChanged();
            }
        }

        public MainViewModel(
            ITourManager tourManager,
            AddTourDialogViewModel addTourDialogViewModel,
            EditTourDialogViewModel editTourDialogViewModel,
            SearchViewModel searchViewModel,
            ResultsViewModel resultsViewModel
        ) {
            this.tourManager = tourManager;
            this.resultsViewModel = resultsViewModel;
            this.editTourDialogViewModel = editTourDialogViewModel;

            SearchTours(null);
            var task = tourManager.getTours();
            TourAsync = new NotifyTaskCompletion<IEnumerable<Tour>>(task);

            AddTourCommand = new RelayCommand((_) => {
                NavigationService?.NavigateTo(addTourDialogViewModel);
            });

            DeleteTourCommand = new RelayCommand((_) => {
                if (SelectedItem == null) {
                    return;
                }

                var task = Task.Run(async () => {
                    try {
                        var res = await tourManager.deleteTour(SelectedItem.TourId);
                        Contract.Requires(res); // checks whether deleting the tour was successful

                        SelectedItem = null;

                        return await tourManager.getTours();
                    } catch (ExternalAccessFailedException e) {
                        throw new FatalException(e.Message);
                    } catch (Exception e) {
                        throw new FatalException($"Error in deleteTour: {e.Message}");
                    }
                });

                TourAsync = new NotifyTaskCompletion<IEnumerable<Tour>>(task);
            }, (_) => SelectedItem != null);

            ExitCommand = new RelayCommand((_) => {
                Close?.Invoke();
            });

            ImportCommand = new RelayCommand((_) => {
                var task = Task.Run(async () => {
                    try {
                        OpenFileDialog openFileDialog = new OpenFileDialog();
                        if (openFileDialog.ShowDialog() == true) {
                            var res = await this.tourManager.importTour(openFileDialog.FileName);
                            Contract.Requires(res); // checks whether deleting the tour was successful
                        }

                        return await tourManager.getTours();
                    } catch (ImportExportException e) {
                        throw new ErrorException(e.Message);
                    } catch (ExternalAccessFailedException e) {
                        throw new FatalException(e.Message);
                    } catch (Exception e) {
                        throw new FatalException($"Error in importTour: {e.Message}");
                    }
                });
                TourAsync = new NotifyTaskCompletion<IEnumerable<Tour>>(task);
            });

            ExportCommand = new RelayCommand((_) => {
                if (SelectedItem == null) {
                    return;
                }

                Guid tourId = SelectedItem.TourId;
                var task = Task.Run(async () => {
                    try {
                        SaveFileDialog saveFileDialog = new SaveFileDialog();
                        if (saveFileDialog.ShowDialog() == true) {
                            var res = await this.tourManager.exportTour(saveFileDialog.FileName, tourId);
                            Contract.Requires(res); // checks whether deleting the tour was successful
                        }

                        return await tourManager.getTours();
                    } catch (ImportExportException e) {
                        throw new ErrorException(e.Message);
                    } catch (ExternalAccessFailedException e) {
                        throw new FatalException(e.Message);
                    } catch (Exception e) {
                        throw new FatalException($"Error in exportTour: {e.Message}");
                    }
                });

                TourAsync = new NotifyTaskCompletion<IEnumerable<Tour>>(task);
            }, (_) => SelectedItem != null);

            ReportTourCommand = new RelayCommand((_) => {
                if (SelectedItem == null) {
                    return;
                }

                Guid tourId = SelectedItem.TourId;
                var task = Task.Run(async () => {
                    try {
                        SaveFileDialog saveFileDialog = new SaveFileDialog();
                        if (saveFileDialog.ShowDialog() == true) {
                            var res = await this.tourManager.generateTourReport(saveFileDialog.FileName, tourId);
                            Contract.Requires(res); // checks whether deleting the tour was successful
                        }

                        return await tourManager.getTours();
                    } catch (ReportGenerationFailedException e) {
                        throw new ErrorException(e.Message);
                    } catch (ExternalAccessFailedException e) {
                        throw new FatalException(e.Message);
                    } catch (Exception e) {
                        throw new FatalException($"Error in generateTourReport: {e.Message}");
                    }
                });

                TourAsync = new NotifyTaskCompletion<IEnumerable<Tour>>(task);
            }, (_) => SelectedItem != null);

            ReportSummarizeCommand = new RelayCommand((_) => {
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                if (saveFileDialog.ShowDialog() == true) {
                    this.tourManager.generateSummarizeReport(saveFileDialog.FileName);
                    TourAsync = new NotifyTaskCompletion<IEnumerable<Tour>>(tourManager.getTours());
                }
            });

            EditTourCommand = new RelayCommand((_) => {
                NavigationService?.NavigateTo(editTourDialogViewModel);
            }, (_) => SelectedItem != null);

            searchViewModel.SearchTextChanged += (_, searchText) => {
                SearchTours(searchText);
            };

            addTourDialogViewModel.TourInputCompleted += (_, tour) => {
                var task = Task.Run(async () => {
                    try {
                        var res = await tourManager.addTour(tour);
                        Contract.Requires(res); // checks whether adding was successful

                        return await tourManager.getTours();
                    } catch (ExternalAccessFailedException e) {
                        throw new FatalException(e.Message);
                    } catch (Exception e) {
                        throw new FatalException($"Error in addTour: {e.Message}");
                    }
                });


                TourAsync = new NotifyTaskCompletion<IEnumerable<Tour>>(task);
            };

            editTourDialogViewModel.TourInputCompleted += (_, tour) => {
                var task = Task.Run(async () => {
                    try {
                        var res = await tourManager.editTour(tour);
                        Contract.Requires(res); // checks whether adding was successful

                        return await tourManager.getTours();
                    } catch (ExternalAccessFailedException e) {
                        throw new FatalException(e.Message);
                    } catch (Exception e) {
                        throw new FatalException($"Error in editTour: {e.Message}");
                    }
                });

                TourAsync = new NotifyTaskCompletion<IEnumerable<Tour>>(task);
            };
        }

        private void SearchTours(string? searchText) {
            var task = tourManager.findMatchingTours(searchText ?? "");
            TourAsync = new NotifyTaskCompletion<IEnumerable<Tour>>(task);
        }
    }
}
