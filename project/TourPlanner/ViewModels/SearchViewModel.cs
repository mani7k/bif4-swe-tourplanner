﻿using System;
using System.Windows.Input;

namespace TourPlanner.ViewModels {
    internal class SearchViewModel : BaseViewModel {
        public event EventHandler<string?>? SearchTextChanged;

        public ICommand SearchCommand { get; }

        public ICommand ClearCommand { get; }

        private string? searchText;
        public string? SearchText {
            get => searchText;
            set {
                searchText = value;
                OnPropertyChanged();
            }
        }

        public SearchViewModel() {
            SearchCommand = new RelayCommand((_) => {
                this.SearchTextChanged?.Invoke(this, SearchText);
            });

            ClearCommand = new RelayCommand((_) => {
                SearchText = "";
                this.SearchTextChanged?.Invoke(this, SearchText);
            });
        }
    }
}
