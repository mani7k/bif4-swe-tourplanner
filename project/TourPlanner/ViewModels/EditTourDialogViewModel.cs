﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using TourPlanner.Models;

namespace TourPlanner.ViewModels {
    internal class EditTourDialogViewModel : BaseViewModel, ICloseWindow {
        public event EventHandler<Tour> TourInputCompleted;
        public ICommand CloseCommand { get; }

        public ICommand CompleteInputCommand { get; }

        public Action? Close { get; set; }

        private Tour selectedItem;
        public Tour SelectedItem {
            get => selectedItem;
            set {
                selectedItem = value;
                OnPropertyChanged();
            }
        }

        public IEnumerable<TransportType> TransportTypeValues {
            get => Enum.GetValues(typeof(TransportType)).Cast<TransportType>();
        }

        public EditTourDialogViewModel() {
            CompleteInputCommand = new RelayCommand((_) => {
                this.TourInputCompleted?.Invoke(this, SelectedItem);
                Close?.Invoke();
            });

            CloseCommand = new RelayCommand((_) => {
                Close?.Invoke();
            });
        }
    }
}
