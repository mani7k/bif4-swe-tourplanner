﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using TourPlanner.Models;

namespace TourPlanner.ViewModels {
    internal class AddTourDialogViewModel : BaseViewModel, ICloseWindow {
        public event EventHandler<Tour> TourInputCompleted;
        public ICommand CloseCommand { get; }

        public ICommand CompleteInputCommand { get; }

        public Action? Close { get; set; }

        public string? Name { get; set; }
        public string? Description { get; set; }
        public string? FromLocation { get; set; }
        public string? ToLocation { get; set; }

        private TransportType selectedTransportType = TransportType.fastest;
        public TransportType SelectedTransportType {
            get { return selectedTransportType; }
            set {
                selectedTransportType = value;
                OnPropertyChanged();
            }
        }

        public IEnumerable<TransportType> TransportTypeValues {
            get => Enum.GetValues(typeof(TransportType)).Cast<TransportType>();
        }

        public AddTourDialogViewModel() {
            CompleteInputCommand = new RelayCommand((_) => {
                Tour tour = new Tour(
                    tourId: Guid.NewGuid(),
                    name: Name,
                    description: Description,
                    fromLocation: FromLocation,
                    toLocation: ToLocation,
                    path: "",
                    estimatedTime: 0,
                    tourDistance: 0,
                    SelectedTransportType
                );

                this.TourInputCompleted?.Invoke(this, tour);
                Close?.Invoke();
            });

            CloseCommand = new RelayCommand((_) => {
                Close?.Invoke();
            });
        }
    }
}
