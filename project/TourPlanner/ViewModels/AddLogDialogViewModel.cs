﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using TourPlanner.Models;

namespace TourPlanner.ViewModels {
    internal class AddLogDialogViewModel : BaseViewModel, ICloseWindow {
        public event EventHandler<TourLog> LogInputCompleted;
        public ICommand CloseCommand { get; }
        public ICommand CompleteInputCommand { get; }
        public Action? Close { get; set; }

        public DateTime? StartingTime { get; set; }
        public string? Comment { get; set; }
        public string? TotalTime { get; set; }
        public int RatingValue { get; set; } = -1;
        public string? Difficulty { get; set; }

        private Diffculty selectedDifficulty = Diffculty.easy;
        public Diffculty SelectedDifficulty {
            get { return selectedDifficulty; }
            set {
                selectedDifficulty = value;
                OnPropertyChanged();
            }
        }

        public IEnumerable<Diffculty> DifficultyValues {
            get => Enum.GetValues(typeof(Diffculty)).Cast<Diffculty>();
        }

        public AddLogDialogViewModel() {
            CompleteInputCommand = new RelayCommand((_) => {
                var tourLog = new TourLog(
                    logid: Guid.NewGuid(),
                    tourId: Guid.NewGuid(),
                    startingTime: (DateTime)StartingTime!,
                    comment: Comment!,
                    totalTime: Convert.ToInt32(TotalTime),
                    rating: RatingValue,
                    difficulty: SelectedDifficulty
                );

                this.LogInputCompleted?.Invoke(this, tourLog);
                Close?.Invoke();
            });

            CloseCommand = new RelayCommand((_) => {
                Close?.Invoke();
            });
        }
    }
}
