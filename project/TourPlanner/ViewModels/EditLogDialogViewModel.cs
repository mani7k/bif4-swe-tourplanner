﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using TourPlanner.Models;

namespace TourPlanner.ViewModels {
    internal class EditLogDialogViewModel : BaseViewModel, ICloseWindow {
        public event EventHandler<TourLog> LogInputCompleted;
        public ICommand CloseCommand { get; }
        public ICommand CompleteInputCommand { get; }
        public Action? Close { get; set; }

        private TourLog selectedItem;
        public TourLog SelectedItem {
            get => selectedItem;
            set {
                selectedItem = value;
                OnPropertyChanged();
            }
        }

        public IEnumerable<Diffculty> DifficultyValues {
            get => Enum.GetValues(typeof(Diffculty)).Cast<Diffculty>();
        }

        public EditLogDialogViewModel() {
            CompleteInputCommand = new RelayCommand((_) => {
                this.LogInputCompleted?.Invoke(this, SelectedItem);
                Close?.Invoke();
            });

            CloseCommand = new RelayCommand((_) => {
                Close?.Invoke();
            });
        }
    }
}
