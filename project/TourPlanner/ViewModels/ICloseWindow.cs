﻿using System;

namespace TourPlanner.ViewModels {
    internal interface ICloseWindow {
        public Action? Close { get; set; }
    }
}
