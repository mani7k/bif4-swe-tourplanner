﻿using System;
using System.Globalization;
using System.Windows.Controls;

namespace TourPlanner.Validation {

    /**
     * This rule checks following requirements:
     * -) value is an int
     * -) value >= 0
     */
    public class MinutesRule : ValidationRule {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo) {
            if (value is null) {
                return new ValidationResult(false, "Please enter a value");
            }

            try {
                var minutes = Int32.Parse(value.ToString());

                if (minutes < 0) {
                    return new ValidationResult(false, "Please enter a positive number");
                }

            } catch (Exception) {
                return new ValidationResult(false, "Invalid input");
            }

            return ValidationResult.ValidResult;
        }
    }
}
