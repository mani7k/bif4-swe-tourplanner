﻿using System.Globalization;
using System.Windows.Controls;

namespace TourPlanner.Validation {
    internal class NotEmptyRule : ValidationRule {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo) {
            if (value is null) {
                return new ValidationResult(false, "Please enter a value");
            }

            if ((value.ToString() ?? "").Trim() == "") {
                return new ValidationResult(false, "Please enter a value");
            }

            return ValidationResult.ValidResult;
        }
    }
}
