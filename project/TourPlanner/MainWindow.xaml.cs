﻿using System.Windows;
using TourPlanner.ViewModels;

namespace TourPlanner {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {
        public MainWindow() {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e) {
            if (DataContext is ICloseWindow cw) {
                cw.Close += () => Close();
            }
        }
    }
}
