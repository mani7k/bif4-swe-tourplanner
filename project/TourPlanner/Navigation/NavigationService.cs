﻿using System;
using System.Collections.Generic;
using System.Windows;
using TourPlanner.ViewModels;

namespace TourPlanner.Navigation {
    internal class NavigationService : INavigationService {
        private readonly Dictionary<Type, Type> viewMapping = new();


        public bool? NavigateTo<TViewModel>(TViewModel viewModel) where TViewModel : BaseViewModel {
            viewModel.NavigationService = this;

            var windowType = viewMapping[typeof(TViewModel)];
            var window = (Window?)Activator.CreateInstance(windowType);

            if (window != null) {
                window.DataContext = viewModel;
                return window.ShowDialog();
            }
            return null;
        }

        public void RegisterNavigation<TViewModel, TWindow>()
            where TViewModel : BaseViewModel
            where TWindow : Window {
            viewMapping[typeof(TViewModel)] = typeof(TWindow);
        }
    }
}
