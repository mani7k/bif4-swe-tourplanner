﻿using TourPlanner.ViewModels;

namespace TourPlanner.Navigation {
    internal interface INavigationService {
        bool? NavigateTo<TViewModel>(TViewModel viewModel) where TViewModel : BaseViewModel;
    }
}
