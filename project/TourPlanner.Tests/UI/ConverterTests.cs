using NUnit.Framework;
using System;
using System.Windows.Data;
using TourPlanner.Converters;

namespace TourPlanner.Tests.UI {
    public class ConverterTests {
        [Test]
        [TestCase("02:00:00", 120.25)]  //cut decimal places
        [TestCase("00:00:00", 0)]
        [TestCase("-02:25:00", -145)]
        public void testTimeDurationConverter(string expectedHours, object minutes) {
            IValueConverter converter = new TimeDurationConverter();
            Assert.AreEqual(expectedHours, converter.Convert(minutes, null, null, null));
        }

        [Test]
        [TestCase("13:27", 13, 27)]
        [TestCase("00:00", 0, 0)]
        public void testStartTimeConverter(string expectedHours, int hours, int minutes) {
            IValueConverter converter = new StartTimeConverter();
            Assert.AreEqual(expectedHours, converter.Convert(new DateTime(2022, 09, 06, hours, minutes, 0), null, null, null));
        }

        [Test]
        [TestCase(0, -2)]   //too small
        [TestCase(40, 0)]   //too big
        public void testStartTimeConverter_ThrowsException_InvalidArgument(int hours, int minutes) {
            IValueConverter converter = new StartTimeConverter();
            Assert.Throws<ArgumentOutOfRangeException>(delegate { converter.Convert(new DateTime(2022, 09, 06, hours, minutes, 0), null, null, null); });
        }

        [Test]
        public void testStartDateConverter() {
            IValueConverter converter = new StartDateConverter();
            Assert.AreEqual("15.12.2022", converter.Convert(new DateTime(2022, 12, 15, 13, 10, 0), null, null, null));
        }

        [Test]
        [TestCase(true, new object[] { })]                                  //none
        [TestCase(true, new object[] { false, false, false })]              //just false
        [TestCase(false, new object[] { false, false, true, false })]      //at least one true
        public void testAndBooleanConverter(bool result, object[] booleans) {
            IMultiValueConverter converter = new AndBooleanConverter();
            Assert.AreEqual(result, converter.Convert(booleans, null, null, null));
        }
    }
}