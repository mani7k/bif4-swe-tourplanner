﻿using NUnit.Framework;
using System;
using TourPlanner.Models;

namespace TourPlanner.Tests.Models {
    public class TourTests {

        [Test]
        [TestCase(-120, 42.2)]                  //negative time
        [TestCase(120, -42.2)]                  //negative distance
        public void testConstructor_throwsException_InvalidArguments(int time, double distance) {
            Assert.Throws<ArgumentException>(delegate {
                new Tour(Guid.NewGuid(), "T1", "Description of T1", "Vienna", "Graz", "C:/...", time, distance, TransportType.fastest);
            });
        }

        [Test]
        [TestCase(5, 0, 1, ChildFriendliness.toddler)]      //difficulties
        [TestCase(0, 300, 1, ChildFriendliness.child)]      //times
        [TestCase(0, 0, 15, ChildFriendliness.adult)]       //distances
        [TestCase(3, 60, 5, ChildFriendliness.child)]       //combination (a Tour with a mediocre difficulty, which is 5km long and takes about an hour can be done by a child)
        public void testCalculateChildFriendliness(double difficultyAvg, double timeAvg, double distance, ChildFriendliness childFriendliness) {
            Tour t = new Tour(Guid.NewGuid(), "T1", "Description of T1", "Vienna", "Graz", "C:/...", 100, distance, TransportType.fastest);
            t.calculateChildFriendliness(difficultyAvg, timeAvg);
            Assert.AreEqual(childFriendliness, t.ChildFriendliness);
        }

        [Test]
        [TestCase(-6, 1)]                   //negative difficulty
        [TestCase(6, 1)]                    //difficulty out of range
        [TestCase(1, -120)]                 //negative time
        public void testCalculateChildFriendliness_ThrowsException_InvalidAverage(double difficultyAvg, double timeAvg) {
            Tour t = new Tour(Guid.NewGuid(), "T1", "Description of T1", "Vienna", "Graz", "C:/...", 100, 42.2, TransportType.fastest);
            Assert.Throws<ArgumentException>(delegate { t.calculateChildFriendliness(difficultyAvg, timeAvg); });
        }


        [Test]
        [TestCase(0, 1)]                    //lower boundary
        [TestCase(10, 3)]                   //inbetween
        [TestCase(50, 5)]                   //upper boundary
        public void testCalculatePopularity(int logCount, int popularity) {
            Tour t = new Tour(Guid.NewGuid(), "T1", "Description of T1", "Vienna", "Graz", "C:/...", 100, 42.2, TransportType.fastest);
            t.calculatePopularity(logCount);
            Assert.AreEqual(popularity, t.Popularity);
        }

        [Test]
        [TestCase(-1)]                      //negative log count
        public void testCalculatePopularity_ThrowsException_InvalidLogCount(int logCount) {
            Tour t = new Tour(Guid.NewGuid(), "T1", "Description of T1", "Vienna", "Graz", "C:/...", 100, 42.2, TransportType.fastest);
            Assert.Throws<ArgumentException>(delegate { t.calculatePopularity(logCount); });
        }
    }
}