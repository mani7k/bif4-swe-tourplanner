﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using TourPlanner.Models;

namespace TourPlanner.Tests.Models {
    public class TourWithLogsTests {
        private Tour tour;
        private List<TourLog> logs;

        [SetUp]
        public void SetUp() {
            tour = new Tour(Guid.NewGuid(), "Test Tour", "Description", "Wien", "Graz", "C:/test/path", 120, 10, TransportType.fastest);

            TourLog l1 = new TourLog(Guid.NewGuid(), tour.TourId, new DateTime(), "Comment", 150, 2, Diffculty.easy);
            TourLog l2 = new TourLog(Guid.NewGuid(), tour.TourId, new DateTime(), "Comment 2", 150, 4, Diffculty.veryHard);
            TourLog l3 = new TourLog(Guid.NewGuid(), tour.TourId, new DateTime(), "Comment 3", 1000, 3, Diffculty.hard);
            TourLog l4 = new TourLog(Guid.NewGuid(), tour.TourId, new DateTime(), "Comment 4", 10, 5, Diffculty.hard);
            TourLog l5 = new TourLog(Guid.NewGuid(), tour.TourId, new DateTime(), "Comment 5", 200, 5, Diffculty.medium);
            logs = new List<TourLog> { l1, l2, l3, l4, l5 };
        }

        [Test]
        public void testSetTourValuesForImport_WithoutLogs() {
            //arrange
            TourWithLogs twl = new TourWithLogs();
            twl.Tour = tour;

            //act
            twl.setTourValuesForImport();

            //assert
            Assert.AreEqual(1, twl.Tour.Popularity);
            Assert.AreEqual(ChildFriendliness.teenage, twl.Tour.ChildFriendliness);

        }

        [Test]
        public void testSetTourValuesForImport_WithLogs() {
            //arrange
            TourWithLogs twl = new TourWithLogs();
            twl.Tour = tour;
            twl.Logs = logs;

            //act
            twl.setTourValuesForImport();

            //assert
            Assert.AreEqual(2, twl.Tour.Popularity);
            Assert.AreEqual(ChildFriendliness.adult, twl.Tour.ChildFriendliness);

        }
    }
}