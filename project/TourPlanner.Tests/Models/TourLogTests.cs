﻿using NUnit.Framework;
using System;
using TourPlanner.Models;

namespace TourPlanner.Tests.Models {
    public class TourLogTests {

        [Test]
        [TestCase(-120, 3)]                     //negative time
        [TestCase(120, -1)]                     //too small rating
        [TestCase(120, 6)]                      //too high rating
        public void testConstructor_throwsException_InvalidArguments(int time, int rating) {
            Assert.Throws<ArgumentException>(delegate {
                new TourLog(Guid.NewGuid(), Guid.NewGuid(), new DateTime(), "Test Log", time, rating, Diffculty.easy);
            });
        }

        [Test]
        public void testToString() {
            Guid logId = Guid.NewGuid();
            Guid tourId = Guid.NewGuid();
            DateTime startingTime = new DateTime();
            string comment = "Test Log";
            int totalTime = 120;
            int rating = 3;
            Diffculty diffculty = Diffculty.easy;

            TourLog log = new TourLog(logId, tourId, startingTime, comment, totalTime, rating, diffculty);
            Assert.AreEqual($"TourLog(logid={logId}, tourId={tourId}, startingTime={startingTime}, comment={comment}, totalTime={totalTime}, rating={rating}, difficulty={diffculty})",
                log.ToString());
        }

    }
}