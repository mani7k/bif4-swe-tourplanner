﻿using iText.IO.Font.Constants;
using iText.IO.Image;
using iText.Kernel.Colors;
using iText.Kernel.Font;
using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using iText.Layout;
using iText.Layout.Borders;
using iText.Layout.Element;
using iText.Layout.Properties;
using Newtonsoft.Json;
using TourPlanner.Models;
using TourPlanner.DAL.Exceptions;

namespace TourPlanner.DAL.Files {
    public class FileDAO : IFileDAO {
        public TourWithLogs importTour(string filename) {
            try {
                using (StreamReader r = new StreamReader(filename)) {
                    string json = r.ReadToEnd();
                    TourWithLogs twl = json.TryParseJson<TourWithLogs>();

                    twl.setTourValuesForImport();
                    return twl;
                }
            } catch (JsonSerializationException ex) {
                throw new ParsingException(ex.Message);
            } catch { 
            }

            return null;
        }

        public bool exportTour(string filename, Tour tour, IEnumerable<TourLog> logs) {
            try {
                TourWithLogs root = new TourWithLogs();
                root.Tour = tour;
                root.Logs = logs.ToList();

                using (StreamWriter file = File.CreateText(filename + ".json")) {
                    JsonSerializer serializer = new JsonSerializer();
                    serializer.Serialize(file, root);
                }
            } catch (Exception) {
                return false;
            }
            return true;
        }

        public bool generateTourReport(string filename, Tour tour, IEnumerable<TourLog> logs) {
            bool res = true;
            Document document = CreateDocument(filename + ".pdf");
            try {
                CreateTourParagraph(ref document, tour);
                if (logs.ToList().Count > 0) {
                    CreateDocumentHeader(ref document, "Logs");
                    CreateLogTable(ref document, logs, false);
                }
            } catch (Exception) {
                res = false;
            } finally {
                document.Close();
            }
            return res;
        }

        public bool generateSummarizeReport(string filename, IEnumerable<TourWithLogs> toursWithLogs) {
            bool res = true;
            if (toursWithLogs.Count() <= 0) {
                return res;
            }
            Document document = CreateDocument(filename + ".pdf");
            try {
                foreach (TourWithLogs tourWithLogs in toursWithLogs) {
                    CreateDocumentHeader(ref document, tourWithLogs.Tour.Name);
                    if (tourWithLogs.Logs.Count > 0) {
                        CreateLogTable(ref document, tourWithLogs.Logs, true);
                    } else {
                        document.Add(new Paragraph("No logs available!"));
                    }
                }

            } catch (Exception) {
                res = false;
            } finally {
                document.Close();
            }
            return res;
        }

        private Document CreateDocument(string fileName) {
            PdfWriter writer = new PdfWriter(fileName);
            PdfDocument pdf = new PdfDocument(writer);
            pdf.SetDefaultPageSize(PageSize.A4.Rotate());
            return new Document(pdf);
        }

        private void CreateDocumentHeader(ref Document document, string title) {
            Paragraph tableHeader = new Paragraph(title)
                    .SetFont(PdfFontFactory.CreateFont(StandardFonts.TIMES_ROMAN))
                    .SetFontSize(18)
                    .SetBold()
                    .SetFontColor(ColorConstants.BLACK);
            document.Add(tableHeader);
        }

        private Cell getHeaderCell(String headerName) {
            return new Cell().Add(new Paragraph(headerName)).SetBold().SetBackgroundColor(ColorConstants.GRAY);
        }

        private Cell getBorderlessCell(String content) {
            return new Cell().Add(new Paragraph(content)).SetBorder(Border.NO_BORDER);
        }


        private void CreateTourParagraph(ref Document document, Tour tour) {
            List list = new List()
                    .SetSymbolIndent(12)
                    .SetListSymbol("\u2022")
                    .SetFont(PdfFontFactory.CreateFont(StandardFonts.TIMES_BOLD));
            list.Add(new ListItem($"From: {tour.FromLocation}"))
                    .Add(new ListItem($"To: {tour.ToLocation}"))
                    .Add(new ListItem($"Estimated Time: {tour.EstimatedTime}min"))
                    .Add(new ListItem($"Distance: {tour.TourDistance}km"))
                    .Add(new ListItem($"Popularity: {tour.Popularity}"))
                    .Add(new ListItem($"Transport Type: {tour.TransportType}"))
                    .Add(new ListItem($"Child Friendliness: {tour.ChildFriendliness}"));

            ImageData imageData = ImageDataFactory.Create(tour.Path);
            Image image = new Image(imageData);
            image.ScaleAbsolute(445, 294);

            CreateDocumentHeader(ref document, tour.Name);
            document.Add(new Paragraph(tour.Description));

            Table table = new Table(UnitValue.CreatePercentArray(2)).UseAllAvailableWidth();
            table.AddCell(list);
            table.AddCell(image);

            document.Add(table);

        }

        private void CreateLogHeader(ref Table table) {
            table.AddHeaderCell(getHeaderCell("Starting Time"));
            table.AddHeaderCell(getHeaderCell("Total Time"));
            table.AddHeaderCell(getHeaderCell("Comment"));
            table.AddHeaderCell(getHeaderCell("Rating"));
            table.AddHeaderCell(getHeaderCell("Difficulty"));
            table.SetFontSize(14).SetBackgroundColor(ColorConstants.WHITE);
        }

        private void CreateLogTable(ref Document document, IEnumerable<TourLog> logs, bool summarize) {
            Table table = new Table(UnitValue.CreatePercentArray(5)).UseAllAvailableWidth();
            CreateLogHeader(ref table);
            foreach (TourLog l in logs) {
                table.AddCell(l.StartingTime.ToString());
                table.AddCell(l.TotalTime.ToString() + "min");
                table.AddCell(l.Comment);
                table.AddCell(l.Rating.ToString());
                table.AddCell(Enum.GetName(typeof(Diffculty), (int)l.Difficulty));
            }

            if (summarize) {
                table.AddCell(getBorderlessCell("Average:").SetBold());
                table.AddCell(logs.Average(l => l.TotalTime).ToString() + "min");
                table.AddCell("");
                table.AddCell(logs.Average(l => l.Rating).ToString());
                table.AddCell(Enum.GetName(typeof(Diffculty), (int)logs.Average(l => (int)l.Difficulty)));
            }

            document.Add(table);
        }
    }
}