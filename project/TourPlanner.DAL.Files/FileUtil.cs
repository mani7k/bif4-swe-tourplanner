﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Schema;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TourPlanner.Models;

namespace TourPlanner.DAL.Files {
   
    public static class FileUtil {
        private static string schema = @" {
    'Tour': {
      'type': 'object',
      'properties': {
        'Name': {
          'type': 'string'
        },
        'Description': {
    'type': 'string'
        },
        'FromLocation': {
    'type': 'string'
        },
        'ToLocation': {
    'type': 'string'
        },
        'Path': {
    'type': 'string'
        },
        'EstimatedTime': {
    'type': 'number'
        },
        'TourDistance': {
    'type': 'number'
        },
        'Popularity': {
    'type': 'integer',
          'minimum': 1,
          'maximum': 5
        },
        'TransportType': {
    'type': 'integer',
          'minimum': 1,
          'maximum': 4
        },
        'ChildFriendliness': {
    'type': 'integer',
          'minimum': 1,
          'maximum': 5
        }
      },
      'required': [
        'Name',
        'Description',
        'FromLocation',
        'ToLocation',
        'Path',
        'EstimatedTime',
        'TourDistance',
        'Popularity',
        'TransportType',
        'ChildFriendliness'
      ]
    },
    'Logs': {
    'type': 'array',
      'items': [
        {
        'type': 'object',
          'properties': {
            'StartingTime': {
                'type': 'string'
            },
            'Comment': {
                'type': 'string'
            },
            'TotalTime': {
                'type': 'integer'
            },
            'Rating': {
                'type': 'integer',
              'minimum': 1,
              'maximum': 5
            },
            'Difficulty': {
                'type': 'integer',
              'minimum': 1,
              'maximum': 4
            }
        },
          'required': [
            'StartingTime',
            'Comment',
            'TotalTime',
            'Rating',
            'Difficulty'
          ]
        }
      ]
    }
  },
  'required': [
    'Tour',
    'Logs'
  ]
}
    ";
        public static TourWithLogs TryParseJson<T>(this string json) {
            JsonSchema parsedSchema = JsonSchema.Parse(schema);
            JObject jObject = JObject.Parse(json);

            return jObject.IsValid(parsedSchema) ?
                JsonConvert.DeserializeObject<TourWithLogs>(json) : null;
        }
    }
}
