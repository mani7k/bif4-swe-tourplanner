﻿namespace TourPlanner.DAL.PostgreSql {
    public interface ITourPostgreSQLDAOConfiguration {
        string ConnectionString { get; }
    }
}
