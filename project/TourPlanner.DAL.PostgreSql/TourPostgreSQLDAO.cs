﻿using Npgsql;
using System.Data;
using TourPlanner.DAL.Exceptions;
using TourPlanner.Logging;
using TourPlanner.Models;

namespace TourPlanner.DAL.PostgreSql {
    public class TourPostgreSQLDAO : ITourDAO {
        private readonly ITourPostgreSQLDAOConfiguration configuration;
        private readonly ILoggerWrapper logger;

        private const string SelectToursCommand = "SELECT tourid, name, description, fromlocation, tolocation, path, estimatedtime, tourdistance, " +
            "(select count(logid) from tourLog tl where tl.TourId = t.TourId) \"logCount\", " +
            "(select coalesce(avg(difficulty),0) from tourLog tl where tl.TourId = t.TourId) \"diffAvg\", " +
            "(select coalesce(avg(totalTime),0) from tourLog tl where tl.TourId = t.TourId) \"timeAvg\", " +
            "transporttype FROM tour t";
        private const string SelectTourByIdCommand = "SELECT tourid, name, description, fromlocation, tolocation, path, estimatedtime, tourdistance, " +
            "(select count(logid) from tourLog tl where tl.TourId = t.TourId) \"logCount\", " +
            "(select coalesce(avg(difficulty),0) from tourLog tl where tl.TourId = t.TourId) \"diffAvg\", " +
            "(select coalesce(avg(totalTime),0) from tourLog tl where tl.TourId = t.TourId) \"timeAvg\", " +
            "transporttype FROM tour t WHERE t.tourid=@tourid";
        private const string InsertTourCommand = "INSERT INTO tour(tourId, name, description, fromlocation, tolocation, path, estimatedtime, tourdistance, transporttype) VALUES (@tourId, @name, @description, @fromlocation, @tolocation, @path, @estimatedtime, @tourdistance, @transporttype)";
        private const string UpdateTourCommand = "UPDATE tour SET name=@name, description=@description, fromlocation=@fromlocation, tolocation=@tolocation, path=@path, estimatedtime=@estimatedtime, tourdistance=@tourdistance, transporttype=@transporttype WHERE tourid=@tourid";
        private const string DeleteTourCommand = "DELETE FROM tour WHERE tourid=@tourid";

        private const string SelectTourLogByTourIdCommand = "SELECT * FROM tourLog WHERE tourId=@tourid";
        private const string SelectTourLogByLogIdCommand = "SELECT * FROM tourLog WHERE logId=@logid";
        private const string InsertLogCommand = "INSERT INTO tourlog(logid, startingtime, comment, totaltime, rating, tourid, difficulty) VALUES (@logid, @startingtime, @comment, @totaltime, @rating, @tourid, @difficulty)";
        private const string UpdateLogCommand = "UPDATE tourlog SET startingtime=@startingtime, comment=@comment, totaltime=@totaltime, rating=@rating, tourid=@tourid, difficulty=@difficulty WHERE logid=@logid";
        private const string DeleteLogCommand = "DELETE FROM tourlog WHERE logid=@logid";

        public TourPostgreSQLDAO(ITourPostgreSQLDAOConfiguration configuration) {
            this.configuration = configuration;
            logger = LoggerFactory.GetLogger();
        }

        //Tours
        public IEnumerable<Tour> getTours() {
            return executeWithConnection(connection => {
                var tours = new List<Tour>();
                using (var cmd = new NpgsqlCommand(SelectToursCommand, connection)) {
                    // take the first row, if any
                    using var reader = cmd.ExecuteReader();
                    while (reader.Read()) {
                        var tour = readTour(reader);
                        tours.Add(tour);
                    }
                }
                return tours;
            });
        }
        public Tour? getTourById(Guid id) {
            return executeWithConnection(connection => {
                Tour? tour = null;
                using (var cmd = new NpgsqlCommand(SelectTourByIdCommand, connection)) {
                    cmd.Parameters.AddWithValue("tourid", id);
                    // take the first row, if any
                    using var reader = cmd.ExecuteReader();
                    if (reader.Read()) {
                        tour = readTour(reader);
                    }
                }
                return tour;
            });
        }
        public bool insertTour(Tour tour) {
            return insertOrUpdateTour(tour, InsertTourCommand);
        }
        public bool updateTour(Tour tour) {
            return insertOrUpdateTour(tour, UpdateTourCommand);
        }
        private bool insertOrUpdateTour(Tour tour, string command) {
            return executeWithConnection(connection => {
                using var cmd = new NpgsqlCommand(command, connection);
                cmd.Parameters.AddWithValue("tourid", tour.TourId);
                cmd.Parameters.AddWithValue("name", tour.Name);
                cmd.Parameters.AddWithValue("description", tour.Description);
                cmd.Parameters.AddWithValue("fromlocation", tour.FromLocation);
                cmd.Parameters.AddWithValue("tolocation", tour.ToLocation);
                cmd.Parameters.AddWithValue("path", tour.Path);
                cmd.Parameters.AddWithValue("estimatedtime", tour.EstimatedTime);
                cmd.Parameters.AddWithValue("tourdistance", tour.TourDistance);
                cmd.Parameters.AddWithValue("transporttype", (int)tour.TransportType);

                var result = cmd.ExecuteNonQuery();

                return result > 0;
            });
        }
        public bool deleteTour(Guid tourId) {
            return executeWithConnection(connection => {
                using var cmd = new NpgsqlCommand(DeleteTourCommand, connection);
                cmd.Parameters.AddWithValue("tourid", tourId);

                var result = cmd.ExecuteNonQuery();
                return result > 0;
            });
        }
        private Tour readTour(IDataRecord record) {
            var tour = new Tour(
                Guid.Parse(Convert.ToString(record["tourid"])),
                Convert.ToString(record["name"]),
                Convert.ToString(record["description"]),
                Convert.ToString(record["fromlocation"]),
                Convert.ToString(record["tolocation"]),
                Convert.ToString(record["path"]),
                Convert.ToInt32(record["estimatedtime"]),
                Convert.ToDouble(record["tourdistance"]),
                (TransportType)Convert.ToInt32(record["transporttype"])
                );
            tour.calculatePopularity(Convert.ToInt32(record["logCount"]));
            tour.calculateChildFriendliness(Convert.ToDouble(record["diffAvg"]), Convert.ToDouble(record["timeAvg"]));

            return tour;
        }

        //Tour Logs
        public IEnumerable<TourLog> getLogsByTourId(Guid tourId) {
            return executeWithConnection(connection => {
                var logs = new List<TourLog>();
                using (var cmd = new NpgsqlCommand(SelectTourLogByTourIdCommand, connection)) {
                    cmd.Parameters.AddWithValue("tourid", tourId);

                    using var reader = cmd.ExecuteReader();
                    while (reader.Read()) {
                        var log = readLog(reader);
                        logs.Add(log);
                    }
                }
                return logs;
            });
        }

        public bool insertLog(TourLog log) {
            return insertOrUpdateLog(log, InsertLogCommand);
        }
        public bool updateLog(TourLog log) {
            return insertOrUpdateLog(log, UpdateLogCommand);
        }
        private bool insertOrUpdateLog(TourLog log, string command) {
            return executeWithConnection(connection => {
                using var cmd = new NpgsqlCommand(command, connection);
                cmd.Parameters.AddWithValue("logid", log.LogId);
                cmd.Parameters.AddWithValue("startingtime", log.StartingTime);
                cmd.Parameters.AddWithValue("comment", log.Comment);
                cmd.Parameters.AddWithValue("totaltime", log.TotalTime);
                cmd.Parameters.AddWithValue("rating", log.Rating);
                cmd.Parameters.AddWithValue("tourid", log.TourId);
                cmd.Parameters.AddWithValue("difficulty", (int)log.Difficulty);

                var result = cmd.ExecuteNonQuery();

                return result > 0;
            });
        }
        public bool deleteLog(Guid logId) {
            return executeWithConnection(connection => {
                using var cmd = new NpgsqlCommand(DeleteLogCommand, connection);
                cmd.Parameters.AddWithValue("logid", logId);

                var result = cmd.ExecuteNonQuery();
                return result > 0;
            });
        }
        private TourLog readLog(IDataRecord record) {
            var log = new TourLog(
                Guid.Parse(Convert.ToString(record["logid"])),
                Guid.Parse(Convert.ToString(record["tourid"])),
                Convert.ToDateTime(record["startingtime"]),
                Convert.ToString(record["comment"]),
                Convert.ToInt32(record["totaltime"]),
                Convert.ToInt32(record["rating"]),
                (Diffculty)Convert.ToInt32(record["difficulty"])
                );

            return log;
        }

        //Helpers
        private T executeWithConnection<T>(Func<NpgsqlConnection, T> command) {
            try {
                using var connection = new NpgsqlConnection(configuration.ConnectionString);
                connection.Open();
                return command(connection);
            } catch (PostgresException e) {
                logger.Error(e.Message);
                throw new DMLException(e.Message);
            } catch (Exception e) {
                logger.Fatal(e.Message);
                throw new DataAccessFailedException(e.Message);
            }
        }
    }
}
