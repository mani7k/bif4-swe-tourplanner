--DDL
drop table if exists TourLog;
drop table if exists Tour;

create table Tour (
    TourId              uuid        primary key
,   Name                text
,   Description         text
,   FromLocation        text
,   ToLocation          text
,   Path                text
,   EstimatedTime       int
,   TourDistance        numeric(6,2)
,   TransportType       int             check(TransportType >= 1 and TransportType <= 4)     
);

create table TourLog (
    LogId               uuid        primary key
,   StartingTime        timestamp   
,   Comment             text
,   TotalTime           int
,   Rating              numeric(2,1)    check(Rating >= 1 and Rating <= 5)  
,   Difficulty          int             check(Difficulty >= 1 and Difficulty <= 4)     
,   TourId              uuid        references Tour ON DELETE CASCADE
);

-- Inserts
insert into tour values ('d66d969e-e24c-11ec-b9d6-87e516b2a926', 'Tour 1', 'Eigentümer von Lord und Saviour Bernhard Weidinger', 'Wien', 'Wien Mitte', 'C:/test/path', 120, 6.5, 1);
insert into tour values ((select uuid_generate_v1()), 'Tour 2', 'Eigentümer von Lord und Saviour Bernhard Weidinger', 'Wien', 'Wien Mitte', 'C:/test/path', 120, 6.5, 1);
insert into tour values ((select uuid_generate_v1()), 'Tour 3', 'Eigentümer von Lord und Saviour Bernhard Weidinger', 'Wien', 'Wien Mitte', 'C:/test/path', 120, 6.5, 1);
insert into tour values ((select uuid_generate_v1()), 'Tour 4', 'Eigentümer von Lord und Saviour Bernhard Weidinger', 'Wien', 'Wien Mitte', 'C:/test/path', 120, 6.5, 1);

insert into tourlog values ((select uuid_generate_v1()), '2022-10-10 09:00', 'hi', 90, 2, 2, 'd66d969e-e24c-11ec-b9d6-87e516b2a926');
insert into tourlog values ((select uuid_generate_v1()), '2022-10-11 10:00', 'naa', 90, 2, 2, 'd66d969e-e24c-11ec-b9d6-87e516b2a926');
insert into tourlog values ((select uuid_generate_v1()), '2022-10-12 9:30', 'be', 90, 2, 2, 'd66d969e-e24c-11ec-b9d6-87e516b2a926');